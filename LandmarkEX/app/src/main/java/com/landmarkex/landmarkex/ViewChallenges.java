package com.landmarkex.landmarkex;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.*;

public class ViewChallenges extends AppCompatActivity {
    private ViewStub stubList;
    private ListView listView;
    private ListViewAdapter listViewAdapter;
    private List<Challenge> challengeList;
    private int currentViewMode = 0;
    private DatabaseReference dbref;



    static final int VIEW_MODE_LISTVIEW = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbref = FirebaseDatabase.getInstance().getReference("challenges");

        challengeList = new ArrayList<>();

        stubList = (ViewStub) findViewById(R.id.stub_list);

        //Inflate ViewStub before get view

        stubList.inflate();

        listView = (ListView) findViewById(R.id.mylistview);

        //get list of product
        getChallengeList();
    }

    private void setAdapters(List<Challenge> updatedChallengeList) {
        if(VIEW_MODE_LISTVIEW == currentViewMode) {
            listViewAdapter = new ListViewAdapter(this, R.layout.list_item, updatedChallengeList);
            listView.setAdapter(listViewAdapter);
        }
    }

    public List<Challenge> getChallengeList() {
        dbref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    challengeList.add(ds.getValue(Challenge.class));
                }
                //Get current view mode in share reference
                SharedPreferences sharedPreferences = getSharedPreferences("ViewMode", MODE_PRIVATE);
                currentViewMode = sharedPreferences.getInt("currentViewMode", VIEW_MODE_LISTVIEW);//Default is view listview
                //Register item lick
                listView.setOnItemClickListener(onItemClick);

                if(VIEW_MODE_LISTVIEW == currentViewMode) {
                    //Display listview
                    stubList.setVisibility(View.VISIBLE);
                }

                setAdapters(challengeList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return challengeList;
    }

    //Ao carregar no Desafio escreve Desafio + descrição
    /*
    AdapterView.OnItemClickListener onItemClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            //Do any thing when user click to item
            Toast.makeText(getApplicationContext(), challengeList.get(position).getName() + " - " + challengeList.get(position).getDesc(), Toast.LENGTH_SHORT).show();
        }
    };
    */
    AdapterView.OnItemClickListener onItemClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            //Do any thing when user click to item
            Intent i = new Intent(view.getContext(), DoChallengeActivity.class);

            i.putExtra("challenge", (Parcelable) challengeList.get(position));
            //i.putExtra()
            startActivity(i);
        }
    };
}
