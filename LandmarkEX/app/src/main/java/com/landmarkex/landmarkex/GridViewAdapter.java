package com.landmarkex.landmarkex;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class GridViewAdapter extends ArrayAdapter<String> {

    public GridViewAdapter(Context context, int resource, List<String> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(null == v) {
            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.grid_item, null);
        }
        //Challenge challenge = getItem(position);
        ImageView img = (ImageView) v.findViewById(R.id.imageView);

        String stampId = getItem(position);
        System.out.println("###################\n "+stampId+"\n#####################");
        //if(!stampId.equals("null")) {
            Picasso.with(this.getContext()).load(stampId).into(img);
        //}
        return v;
    }
}
