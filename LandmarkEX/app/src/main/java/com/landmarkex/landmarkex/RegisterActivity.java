package com.landmarkex.landmarkex;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {

    private TextInputLayout mUsername;
    private TextInputLayout mEmail;
    private TextInputLayout mPassword;

    private Button mRegButton;

    //Progress Dialog
    private ProgressDialog mRegProgress;

    //Firebase
    private FirebaseAuth mAuth;
    private DatabaseReference mDataBaseRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mRegProgress = new ProgressDialog(this);

        //Firebase
        mAuth=FirebaseAuth.getInstance();

        mUsername = (TextInputLayout) findViewById(R.id.reg_name);
        mEmail = (TextInputLayout) findViewById(R.id.login_email);
        mPassword= (TextInputLayout) findViewById(R.id.login_password);
        mRegButton= (Button) findViewById(R.id.reg_button_create);

        mRegButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = mUsername.getEditText().getText().toString();
                String email = mEmail.getEditText().getText().toString();
                String password = mPassword.getEditText().getText().toString();
                //String image = "defaultprofile.png";

                if(!TextUtils.isEmpty(username)&&!TextUtils.isEmpty(email)&&!TextUtils.isEmpty(password)) {

                    mRegProgress.setTitle("Registering User");
                    mRegProgress.setMessage("Please wait while we register you");
                    mRegProgress.setCanceledOnTouchOutside(false);
                    mRegProgress.show();
                    register_user(username,email,password);}


            }
        });
    }

    private void register_user(final String username, String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            FirebaseUser current_user=FirebaseAuth.getInstance().getCurrentUser();
                            String uid = current_user.getUid();

                            mDataBaseRef=FirebaseDatabase.getInstance().getReference().child("users").child(uid);

                            HashMap<String, String> userMap=new HashMap<>();
                            userMap.put("username", username);
                            userMap.put("stamps","0");
                            userMap.put("stampIds", "null");
                            userMap.put("score","0");
                            userMap.put("profile_image","https://firebasestorage.googleapis.com/v0/b/landmarkex-1.appspot.com/o/profile_images%2Fdefaultprofile.png?alt=media&token=74fda727-4ed4-41f0-b863-feb1cca5905e");

                            mDataBaseRef.setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful()){
                                        mRegProgress.dismiss();
                                        Intent mapsIntent = new Intent(RegisterActivity.this, MapsMenu.class);
                                        startActivity(mapsIntent);
                                        finish();

                                    }
                                }
                            });


                        } else {
                            mRegProgress.hide();
                            // If sign in fails, display a message to the user.
                            //Log.w("TAG", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(RegisterActivity.this, "Registering failed.",
                                    Toast.LENGTH_SHORT).show();
                            //updateUI(null);
                        }

                    }
                });
    }

    @Override
    public void onBackPressed() {
        Intent homeintent = new Intent (RegisterActivity.this, Home.class);
        startActivity(homeintent);
        finish();
    }
}
