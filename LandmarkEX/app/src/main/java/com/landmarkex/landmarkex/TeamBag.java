package com.landmarkex.landmarkex;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;

public class TeamBag implements Serializable{

    private ConcurrentHashMap<String, Team> bag;
    //private ArrayList<String> users;
    private static TeamBag tbag = new TeamBag();


    private TeamBag(/*ArrayList<String> users*/){
        if(bag == null) {
            this.bag = new ConcurrentHashMap<>();
    //        this.users = users;
        }
    }

    public static TeamBag getInstance(){
       return tbag;
    }

    public void put(String key, Team value){
        this.bag.put(key, value);
    }

    public Team get(String key){
        return this.bag.get(key);
    }

/*
    private ArrayList<Team> t;
    private static TeamBag tbag = new TeamBag();

    private TeamBag(){
        if()
    }*/
}
