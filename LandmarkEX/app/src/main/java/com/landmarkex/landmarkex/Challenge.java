package com.landmarkex.landmarkex;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Challenge implements Parcelable{

    private String answer;
    private String creator;
    private String desc;
    private String name;
    private int points;
    private String question;
    private ChallengeType type;
    private String img;
    private double lat;
    private double lng;


    public Challenge(String creator, String name, String desc, int points, ChallengeType type,
                     String question, String answer, String img, double lat, double lng) {
        this.creator = creator;
        this.name = name;
        this.desc = desc;
        this.points = points;
        this.type = type;
        this.question = question;
        this.answer = answer;
        this.img = img;
        this.lat = lat;
        this.lng = lng;
    }

    public Challenge() {}

    public Challenge(Parcel in) {
        this.answer = in.readString();
        this.creator = in.readString();
        this.desc = in.readString();
        this.name = in.readString();
        this.points = in.readInt();
        this.question = in.readString();
        this.type = (ChallengeType) in.readValue(ChallengeType.class.getClassLoader());
        this.img = in.readString();
        this.lat = in.readDouble();
        this.lng = in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(answer);
        parcel.writeString(creator);
        parcel.writeString(desc);
        parcel.writeString(name);
        parcel.writeInt(points);
        parcel.writeString(question);
        parcel.writeValue(type);
        parcel.writeString(img);
        parcel.writeDouble(lat);
        parcel.writeDouble(lng);
    }

    public static final Parcelable.Creator<Challenge> CREATOR = new Parcelable.Creator<Challenge>() {
        public Challenge createFromParcel(Parcel in) {
            return new Challenge(in);
        }

        public Challenge[] newArray(int size) {
            return new Challenge[size];
        }
    };

    public String getAnswer() {
        return answer;
    }

    public String getCreator() {
        return creator;
    }

    public String getDesc() {
        return desc;
    }

    public String getName() {
        return name;
    }

    public int getPoints() {
        return points;
    }

    public String getQuestion() {
        return question;
    }

    public String getImg() {
        return img;
    }

    /*public LatLng getLatlng() {
        return latlng;
    }*/

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
