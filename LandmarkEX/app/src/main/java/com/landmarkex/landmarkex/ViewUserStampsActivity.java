package com.landmarkex.landmarkex;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewStub;
import android.widget.GridView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ViewUserStampsActivity extends AppCompatActivity {

    private GridViewAdapter gridViewAdapter;
    private GridView gridView;
    private ViewStub stubGrid;
    private ArrayList<String> stampList;
    private DatabaseReference dbref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_user_stamps);

        dbref = FirebaseDatabase.getInstance()
                .getReference("users")
                .child(FirebaseAuth.getInstance().getUid())
                .child("stampIds");

        stampList = new ArrayList<>();

        stubGrid = (ViewStub) findViewById(R.id.stub_grid);

        stubGrid.inflate();

        gridView = (GridView) findViewById(R.id.mygridview);

        //stubGrid.setVisibility(View.VISIBLE);

        getStampIds();

        //gridViewAdapter = new GridViewAdapter(this, R.layout.grid_item, stampList);
        //gridView.setAdapter(gridViewAdapter);


    }

    private void setAdapters(List<String> updatedChallengeList) {
        gridViewAdapter = new GridViewAdapter(this, R.layout.grid_item, stampList);
        gridView.setAdapter(gridViewAdapter);
    }


    private List<String> getStampIds(){
        dbref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    stampList.add(ds.getValue(String.class));
                }
                stubGrid.setVisibility(View.VISIBLE);
                setAdapters(stampList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return stampList;
    }
}
