package com.landmarkex.landmarkex;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class DoChallengeActivity extends AppCompatActivity {

    private Challenge challenge;
    private EditText answer;

    private DatabaseReference dbref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_do_challenge);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.challenge = (Challenge) getIntent().getParcelableExtra("challenge");
        TextView question = findViewById(R.id.dochallenge_question);
        question.setText(this.challenge.getQuestion());

        this.answer = (EditText) findViewById(R.id.dochallenge_answer);

    }

    public void completeChallenge(View v) {
        if (this.challenge.getAnswer().equals(this.answer.getText().toString())) {
            this.dbref = FirebaseDatabase.getInstance()
                    .getReference("users")
                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid());

/*
            this.dbref.orderByChild("creator")
                    .equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid())
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            for (DataSnapshot ds : dataSnapshot.getChildren()){
                                Challenge c = ds.getValue(Challenge.class);
                                if(c.getQuestion().equals(challenge.getQuestion())){

                                    break;
                                }

                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
*/

            this.dbref.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    int stampCount = Integer.parseInt(dataSnapshot.child("stamps").getValue().toString());
                    int score = Integer.parseInt(dataSnapshot.child("score").getValue().toString());

                    dbref.child("stamps").setValue(stampCount + 1);
                    dbref.child("score").setValue(score + 1);
                    dbref.child("stampIds").push().setValue(challenge.getImg());

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
            Intent i = new Intent(this, MapsMenu.class);
            startActivity(i);
        } else {
            Toast.makeText(getApplicationContext(), "Resposta errada, tente novamente.", Toast.LENGTH_SHORT).show();
        }
    }
}
