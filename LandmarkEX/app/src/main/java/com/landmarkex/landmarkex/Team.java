package com.landmarkex.landmarkex;

import java.io.Serializable;
import java.util.ArrayList;

public class Team implements Serializable{

    private ArrayList<String> users;
    private String teamName;

    public Team(ArrayList<String> users, String teamName) {
        this.users = users;
        this.teamName = teamName;
    }
}
