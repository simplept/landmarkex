package com.landmarkex.landmarkex;

/**
 * Created by inesq on 12/12/2017.
 */

public class UserAccountSettings {

    private String profile_photo;
    private int score;
    private int stamps;
    private String username;

    public UserAccountSettings(String profile_photo, int score,
                               int stamps, String username) {

        this.profile_photo = profile_photo;
        this.score = score;
        this.stamps = stamps;
        this.username = username;
    }

    public UserAccountSettings() {


    }

    public String getProfile_photo() {
        return profile_photo;
    }

    public void setProfile_photo(String profile_photo) {
        this.profile_photo = profile_photo;
    }

    public long getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public long getStamps() {
        return stamps;
    }

    public void setStamps(int stamps) {
        this.stamps = stamps;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "UserAccountSettings{" +
                ", profile_photo='" + profile_photo + '\'' +
                ", score=" + score +
                ", stamps=" + stamps +
                ", username=" + username +
                '}';
    }
}
