package com.landmarkex.landmarkex;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class ListViewAdapter extends ArrayAdapter<Challenge> {


    public ListViewAdapter(Context context, int resource, List<Challenge> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(null == v) {
            LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.list_item, null);
        }

        Challenge challenge = getItem(position);
        TextView txtTitle = (TextView) v.findViewById(R.id.txtTitle);
        TextView txtDescription = (TextView) v.findViewById(R.id.txtDescription);

        ImageView img = (ImageView) v.findViewById(R.id.imageView);

        Picasso.with(this.getContext()).load(challenge.getImg()).into(img);
        txtTitle.setText(challenge.getName());
        txtDescription.setText(challenge.getDesc());
        return v;
    }
}
