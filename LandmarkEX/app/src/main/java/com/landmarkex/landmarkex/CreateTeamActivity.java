package com.landmarkex.landmarkex;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;

public class CreateTeamActivity extends AppCompatActivity {

    private ArrayList<String> listItems = new ArrayList<>();
    private ArrayAdapter<String> adapter;
    private EditText userToBeAdded;
    private EditText teamName;
    //private String loggedUser;
    private DatabaseReference dbref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_team);

        dbref = FirebaseDatabase.getInstance().getReference("users");

/*
        Intent i = getIntent();
        this.loggedUser = i.getStringExtra("user");
        this.listItems.add(this.loggedUser);
        //TODO review this
        TextView tv = (TextView) findViewById(R.id.createteam_textView_challengename);
        tv.setText(this.loggedUser);
*/
        this.userToBeAdded = (EditText) findViewById(R.id.createteam_plaintext_friendname);
        this.teamName = (EditText) findViewById(R.id.createteam_plaintext_teamname);

        ListView lv = (ListView) findViewById(R.id.createteam_listview);
        this.adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listItems);
        lv.setAdapter(this.adapter);
    }

    public void addUser(View v) {
        if(!this.listItems.contains(this.userToBeAdded.getText().toString()) &&
                dbref.equalTo(this.userToBeAdded.getText().toString()) != null) {
            this.listItems.add(this.userToBeAdded.getText().toString());
            this.adapter.notifyDataSetChanged();
        }
    }

    /*
        public void removeUser(View v){

        }
    */
    public void createTeam(View v) {
        TeamBag t = TeamBag.getInstance(/*this.listItems*/);
        t.put(this.teamName.getText().toString(), new Team(this.listItems, this.teamName.getText().toString()));

        Intent i = new Intent(this, MapsMenu.class);
        i.putExtra("users", this.listItems);
        i.putExtra("teamName", this.teamName.getText().toString());
        startActivity(i);
    }
}
