package com.landmarkex.landmarkex;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.theartofdev.edmodo.cropper.CropImage;

public class CreateChallenge extends AppCompatActivity {

    private EditText challengeName;
    private EditText challengeDesc;
    private EditText question;
    private EditText answer;
    private String spinnerChoice;
    private double lat;
    private double lng;
    private Button choose_stamp;
    private static final int GALLERY_PICK=1;

    private DatabaseReference db;
    private StorageReference mImageStorage;
    private ProgressDialog mProgressDialog;
    private String download_url;

    //TODO FAZER ISTO
    //@SuppressLint("ResourceType")
    private String img_stamp="https://firebasestorage.googleapis.com/v0/b/landmarkex-1.appspot.com/o/profile_images%2Fdefaultprofile.png?alt=media&token=74fda727-4ed4-41f0-b863-feb1cca5905e";
    private int stampId = R.drawable.ic_menu_gallery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_challenge);

        //img_stamp=(ImageView) findViewById(R.id.imageView2);
        choose_stamp = (Button) findViewById(R.id.createchallenge_buttonchoosestamp);
        mImageStorage = FirebaseStorage.getInstance().getReference();
        choose_stamp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent galleryIntent = new Intent();
                galleryIntent.setType("image/*");
                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(galleryIntent, "SELECT_IMAGE"), GALLERY_PICK);

            }
        });

        Intent i = getIntent();
        this.lat = i.getDoubleExtra("lat", 0.0);
        this.lng = i.getDoubleExtra("lng", 0.0);

        this.challengeName = (EditText) findViewById(R.id.createchallenge_name);

        final Spinner s = (Spinner) findViewById(R.id.createchallenge_choosetypeofcha);

        this.spinnerChoice = s.getSelectedItem().toString();
        //if (this.spinnerChoice.equals(ChallengeType.Pergunta)){
        this.question = (EditText) findViewById(R.id.createchallenge_question);
        this.answer = (EditText) findViewById(R.id.createchallenge_answer);

        this.challengeDesc = (EditText) findViewById(R.id.createchallenge_challengedesc);
    }

    public void chooseStamp(View v) {

    }

    public void createChallenge(View v) {

        String name = this.challengeName.getText().toString();
        String desc = this.challengeDesc.getText().toString();

        ChallengeType type = this.spinnerChoice.toLowerCase().equals("pergunta") ? ChallengeType.Pergunta : ChallengeType.Puzzle;
        String imagepath = this.img_stamp = download_url;
        System.out.println(download_url);
        Challenge c = new Challenge(FirebaseAuth.getInstance().getCurrentUser().getUid()
                , name, desc, 2, type, question.getText().toString(),
                        answer.getText().toString(), imagepath, /*stampId,*//*stamp.getId()*/lat, lng/*latlng*/);

        db = FirebaseDatabase.getInstance().getReference();
        db.child("img_stamp").setValue(download_url).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    mProgressDialog.dismiss();
                }
            }
        });
        db.child("challenges").push().setValue(c);

        Intent i = new Intent(this, MapsMenu.class);
        startActivity(i);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_PICK && resultCode == RESULT_OK) {

            Uri imageUri = data.getData();

            CropImage.activity(imageUri).setRequestedSize(120,120)
                    .setAspectRatio(1, 1)
                    .start(this);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {

                Uri resultUri = result.getUri();

                //StorageReference filepath = mImageStorage.child("challenge_images").child(FirebaseAuth.getInstance().getCurrentUser().getUid()+this.challengeName.getText().toString()+this.challengeDesc.getText().toString()+".bitmap");
                StorageReference filepath = mImageStorage.child("challenge_images")
                        .child(FirebaseAuth.getInstance().getCurrentUser().getUid() +
                                this.challengeName.getText().toString() +
                                this.challengeDesc.getText().toString()+".bitmap");
                filepath.putFile(resultUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if (task.isSuccessful()) {
                    download_url=task.getResult().getDownloadUrl().toString();
                } else {
                    Toast.makeText(CreateChallenge.this, "Error in uploading", Toast.LENGTH_SHORT).show();
                    mProgressDialog.dismiss();
                }
            }
        });

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }
}
