package com.landmarkex.landmarkex;

/**
 * Created by carlosn on 17/11/2017.
 */

public enum ChallengeType {
    Pergunta,
    Puzzle
}
